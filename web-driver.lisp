;;    Common Lisp Web Driver
;;    Copyright (C) 2021  Abigale Raeck

;;    This program is free software: you can redistribute it and/or modify
;;    it under the terms of the GNU General Public License as published by
;;    the Free Software Foundation, either version 3 of the License, or
;;    (at your option) any later version.

;;    This program is distributed in the hope that it will be useful,
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;    GNU General Public License for more details.

;;    You should have received a copy of the GNU General Public License
;;    along with this program.  If not, see <https://www.gnu.org/licenses/>.


(mapc #'quicklisp:quickload
      '(dexador
        cl-base64
        cl-arrows
        cl-ppcre
        jonathan))

(defpackage web-driver
  (:use cl-arrows common-lisp)
  (:nicknames wd)
  (:export

   web-driver-error
   WEB-DRIVER/INVALID-SELECTOR
   WEB-DRIVER/INVALID-ELEMENT-STATE
   WEB-DRIVER/INVALID-COOKIE-DOMAIN
   WEB-DRIVER/INVALID-ARGUMENT
   WEB-DRIVER/INSECURE-CERTIFICATE
   WEB-DRIVER/ELEMENT-NOT-INTERACTABLE
   WEB-DRIVER/ELEMENT-CLICK-INTERCEPTED
   WEB-DRIVER/UNKNOWN-COMMAND
   WEB-DRIVER/STALE-ELEMENT-REFERENCE
   WEB-DRIVER/NO-SUCH-WINDOW
   WEB-DRIVER/NO-SUCH-FRAME
   WEB-DRIVER/NO-SUCH-ELEMENT
   WEB-DRIVER/NO-SUCH-COOKIE
   WEB-DRIVER/NO-SUCH-ALERT
   WEB-DRIVER/INVALID-SESSION-ID
   WEB-DRIVER/UNKNOWN-METHOD
   WEB-DRIVER/UNSUPPORTED-OPERATION
   WEB-DRIVER/UNKNOWN-ERROR
   WEB-DRIVER/UNEXPECTED-ALERT-OPEN
   WEB-DRIVER/UNABLE-TO-CAPTURE-SCREEN
   WEB-DRIVER/UNABLE-TO-SET-COOKIE
   WEB-DRIVER/TIMEOUT
   WEB-DRIVER/SESSION-NOT-CREATED
   WEB-DRIVER/SCRIPT-TIMEOUT
   WEB-DRIVER/MOVE-TARGET-OUT-OF-BOUNDS
   WEB-DRIVER/JAVASCRIPT-ERROR

   
   get-firefox-profile-base64
   *web-driver*
   *active-session*
   web-driver-responce-error
   <web-driver>
   session-create
   session-delete
   current-status
   session-timeouts
   session-change-timeouts
   navigate-to-url
   navigate-current-url
   navigate-back
   navigate-forward
   navigate-refresh
   session-current-title
   window-get-handle
   window-get-handles
   element-get-active
   element-find
   elements-find
   element-find-from-element
   elements-find-from-element
   element-selected?
   element-attribute
   element-property
   element-text
   element-click
   page-source
   execute-script))

(in-package :web-driver)

;;cl-base64

(defun get-firefox-profile-base64 ()
  (with-open-file (zip-stream "firefox-profile.zip" :direction :input :element-type '(unsigned-byte 8))
    (cl-base64:usb8-array-to-base64-string (let ((data (make-array (file-length zip-stream) :element-type '(unsigned-byte 8))))
                                             (read-sequence data zip-stream)
                                             data))))

                                        ; Class Web Driver
(defclass <web-driver> ()
  ((base-uri :initform "http://localhost:4444"
             :initarg :base-uri
             :reader wd-base-uri)
   (active-sessions :initform '()
                    :reader wd-get-active-sessions)))

(defvar *web-driver* (make-instance '<web-driver>))
(defvar *active-session* nil)

(defmethod print-object ((obj <web-driver>) stream)
  (format stream "~&<webdrive>r~% - base-uri: ~s~% - active-sessions: ~s" (wd-base-uri obj) (slot-value obj 'active-sessions)))

(defun add-session (session-id capabilities)
  (setf (slot-value *web-driver* 'active-sessions)
        (acons session-id capabilities (slot-value *web-driver* 'active-sessions)))
  (unless *active-session*
    (setf *active-session* session-id)))

(defun remove-session (session-id)
  (setf (slot-value *web-driver* 'active-sessions)
        (remove session-id (slot-value *web-driver* 'active-sessions) :test #'equal :key #'car))
  (when (equal *active-session* session-id)
    (setf *active-session* (when (car (wd-get-active-sessions *web-driver*))
                             (caar (wd-get-active-sessions *web-driver*))))))


                                        ; Conditionals/Errors
(define-condition web-driver-responce-error ()
  ((used-url :initarg :uri
             :reader web-driver-responce-error-url)
   (error-type :initarg :error
               :reader web-driver-responce-error-type)
   (error-msg :initarg :error
              :reader web-driver-responce-error-msg)
   (stack-trace :initarg :stack-trace
                :reader web-driver-responce-error-stack-trace))
  (:report (lambda (condition stream)
             (format stream "call to \"~a\" responded with error ~a: ~a~2%=======StackTrace=======~%~a"
                     (web-driver-responce-error-url condition)
                     (web-driver-responce-error-type condition)
                     (web-driver-responce-error-msg condition)
                     (web-driver-responce-error-stack-trace condition)))))

;; 6.6 Errors
;; '4xx '5xx error codes (400 client errors) (500 server errors)
;; error, msg, stacktrace, data (sometimes)
#|
console.log("(" +  $x("/html/body/section[8]/section[6]/table/tbody/tr").reduce((o, r) => {
  return o + "\n" + `(${r.children[1].innerText} . "${r.children[2].innerText}")`
}, "") + ")")
|#

(defvar *error-code-lookup* (make-hash-table :test #'equal))

(define-condition web-driver-error ()
  ((code
    :initarg :code
    :reader web-driver-error-code
    :type string)
   (message
    :initarg :message
    :reader web-driver-error-message
    :type string)
   (stacktrace
    :initarg :stacktrace
    :reader web-driver-error-stacktrace
    :type string)
   (data
    :initarg :data
    :reader web-driver-error-data)))

(defmacro create-web-driver-error (name namestr)
  `(define-condition ,name (web-driver-error)
     ()
     (:default-initargs :code ,namestr)
     (:report (lambda (condition stream)
                (format stream "~a~%~a~%~a"
                        (web-driver-error-message condition)
                        (web-driver-error-stacktrace condition)
                        (web-driver-error-data condition))))))

(defmacro create-web-driver-error-translator ())

#.`(progn
     ,@(let ((web-driver-codes '((400 . "element click intercepted")
                                 (400 . "element not interactable")
                                 (400 . "insecure certificate")
                                 (400 . "invalid argument")
                                 (400 . "invalid cookie domain")
                                 (400 . "invalid element state")
                                 (400 . "invalid selector")
                                 (404 . "invalid session id")
                                 (500 . "javascript error")
                                 (500 . "move target out of bounds")
                                 (404 . "no such alert")
                                 (404 . "no such cookie")
                                 (404 . "no such element")
                                 (404 . "no such frame")
                                 (404 . "no such window")
                                 (500 . "script timeout")
                                 (500 . "session not created")
                                 (404 . "stale element reference")
                                 (500 . "timeout")
                                 (500 . "unable to set cookie")
                                 (500 . "unable to capture screen")
                                 (500 . "unexpected alert open")
                                 (404 . "unknown command")
                                 (500 . "unknown error")
                                 (405 . "unknown method")
                                 (500 . "unsupported operation")
                                 (500 . "javascript error"))))
         `(,@(loop for (code . name) in web-driver-codes
                   for sym = (->> (cl-ppcre:regex-replace-all " " name "-")
                                  (concatenate 'string "web-driver/")
                                  string-upcase
                                  intern)
                   collect `(progn (create-web-driver-error ,sym ,name)
                                   (setf (gethash ,name *error-code-lookup*) (quote ,sym)))))))


                                        ; Endpoint Conections
(defmacro connect-to-endpoint
    (fn-call
     endpoint-template
     &key
       (pre-action-bind '())
       (action 'body)
       (http-request-type 'get)
       http-body)
  (let* (template-vars
         (format-template
          (ppcre:regex-replace-all "\\{\\S+?\\}" endpoint-template
                                   (lambda (match start end mstart mend &rest rest)
                                     (declare (ignore start) (ignore end) (ignore rest))
                                     (setf template-vars (cons
                                                          (intern (string-upcase (subseq match (+ 1 mstart) (- mend 1))))
                                                          template-vars))
                                     "~a"))))
    `(,@fn-call
      (let* (,@pre-action-bind
             (call-uri ,`(format nil ,format-template ,@(reverse template-vars)))
             (rbody (handler-bind ((dex:http-request-not-found #'dex:ignore-and-continue)
                                   (dex:http-request-bad-request #'dex:ignore-and-continue)                                   
                                   (dex:http-request-method-not-allowed #'dex:ignore-and-continue)
                                   (dex:http-request-internal-server-error #'dex:ignore-and-continue))
                      (dex:request (concatenate 'string (wd-base-uri *web-driver*) call-uri)
                                   :method ',http-request-type
                                   ,@(when http-body `(:content ,http-body)))))
             (jbody (getf (jonathan:parse rbody) :|value|))
             (body (if (when (listp jbody) (find :|error| jbody))
                       (let ((error-sym (gethash (getf jbody :|error|) *error-code-lookup*)))
                         (apply #'error `(,(or error-sym 'web-driver-error)
                                          ,@(unless error-sym (list web-driver-error))
                                          :msg ,(getf jbody :|message|)
                                          :stack-trace ,(getf jbody :|stacktrace|)
                                          :data ,(getf jbody :|data|))))
                       jbody)))
        ,action))))

#|(connect-to-endpoint (defun wd-create-session (wd &optional session-id))
  "/session"
  :pre-action-bind   ;; aditional lets/bindings?  
  :http-request-type ;; get post delete ....
  :http-body         ;; json string ....
  :action            ;; use body
)|#

                                        ; https://www.w3.org/TR/2020/WD-webdriver2-20200824/
                                        ; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;; 8.1 New Session
(connect-to-endpoint
 (defun session-create (&optional capabilities))
 "/session"
 :http-request-type post
 :http-body (jonathan.encode:to-json `(:obj (:|capabilities| . (:obj ,@capabilities))) :from :jsown)
 :action
 (let ((session-id (getf body :|sessionId|))
        (remote-capabilities (getf body :|capabilities|)))
   (add-session session-id remote-capabilities)
   (cons
    (getf body :|sessionId|)
    (getf body :|capabilities|))
  (format t "~%~s~%" (jonathan.encode:to-json `(:obj (:|capabilities| . (:obj ,@capabilities))) :from :jsown))))

;; 8.2 Delete Session
(connect-to-endpoint
 (defun session-delete ())
 "/session/{*active-session*}"
 :http-request-type delete
 :action
 (remove-session *active-session*))

;; 8.3 Status
(connect-to-endpoint
 (defun current-status ()
   "gets current session status and returns a plist containg :|ready| :|message| keys")
 "/status")

;; 9.1 Get Timeouts
;;; todo check
(connect-to-endpoint
 (defun session-timeouts ())
 "/session/{*active-session*}/timeouts")

;; 9.2 Set Timeouts
;;; todo check
(connect-to-endpoint
 (defun session-change-timeouts (&key (script 30000) (page-load 300000) (implicit 0)))
 "/session/{*active-session*}/timeouts"
 :http-request-type post
 :http-body (jonathan.encode:to-json `(:obj
                                       (:|script| . ,script)
                                       (:|pageLoad| . ,page-load)
                                       (:|implicit| . ,implicit))
              :from :jsown))

;; 10.1 Go
(connect-to-endpoint
 (defun navigate-to-url (url)
   (declare (string url)))
 "/session/{*active-session*}/url"
 :http-request-type post
 :http-body (jonathan.encode:to-json `(:obj (:|url| . ,url)) :from :jsown)
 :action t)

;; 10.2 Get Current URL
(connect-to-endpoint
 (defun navigate-current-url ()
   "retreives the current url of session. Returns String")
 "/session/{*active-session*}/url")

;; 10.3 Back
(connect-to-endpoint
 (defun navigate-back ()
   "Navigates back to previous url (even if invald, will through error in that case)")
 "/session/{*active-session*}/back"
 :http-request-type post
 :http-body "{}"
 :action t)

;; 10.4 Forward
(connect-to-endpoint
 (defun navigate-forward ()
   "Navigates forward to next url (even if invald, will through error in that case)")
 "/session/{*active-session*}/forward"
 :http-request-type post
 :http-body "{}"
 :action t)

;; 10.5 Refresh
(connect-to-endpoint
 (defun navigate-refresh ())
 "/session/{*active-session*}/refresh"
 :http-request-type post
 :http-body "{}"
 :action t)

;; 10.6 Get Title
(connect-to-endpoint
 (defun session-current-title ()
   "Retrevies the current page title. Returns String")
 "/session/{*active-session*}/title")

;; 11.1 Get Window Handle
(connect-to-endpoint
 (defun window-get-handle ()
   "gets current window handle Returns String ID")
 "/session/{*active-session*}/window")

;; 11.2 Close Window
(connect-to-endpoint
 (defun window-close-current ())
 "/session/{*active-session*}/window"
 :http-request-type delete)

;; 11.3 Switch To Window
(connect-to-endpoint
 (defun window-switch (window-handle))
 "/session/{*active-session*}/window"
 :http-request-type post
 :http-body (jonathan.encode:to-json `(:obj (:|handle| . ,window-handle)) :from :jsown)
 :action t)

;; 11.4 Get Window Handles
(connect-to-endpoint
 (defun window-get-handles ()
   "Gets a list of available window handles")
 "/session/{*active-session*}/window/handles")

;; 11.5 New Window
;;;todo check
(connect-to-endpoint
 (defun window-new ())
 "/session/{*active-session*}/window/new"
 :http-request-type post)

;; 11.6 Switch To Frame
(connect-to-endpoint
 (defun frame-switch (&optional frame-id)
   (declare (string frame-id)))
 "/session/{*active-session*}/frame"
 :http-request-type post
 :http-body (if frame-id
                (jonathan.encode:to-json `(:obj (:|id| . ,frame-id)) :from :jsown)
                "{}"))

;; 11.7 Switch To Parent Frame
(connect-to-endpoint
 (defun frame-to-parent ())
 "/session/{*active-session*}/frame/parent"
 :http-request-type post
 :http-body "{}"
 :action t)

;; 11.8.1 Get Window Rect
(connect-to-endpoint
 (defun window-get-rect ()
   "Gets current window size. Returns plist with :|height| :|width| :|y| :|x|")
 "/session/{*active-session*}/window/rect")

;; 11.8.2 Set Window Rect
(connect-to-endpoint
 (defun window-set-rect (&key height width x y)
   "Sets window size")
 "/session/{*active-session*}/window/rect"
 :http-request-type post
 :http-body (jonathan.encode:to-json `(:obj ,(when height `(:|height| . ,height))
                                            ,(when width `(:|width| . ,width))
                                            ,(when x `(:|x| . ,x))
                                            ,(when y `(:|y| . ,y)))
                                     :from :jsown))

;; 11.8.3 Maximize Window
(connect-to-endpoint
 (defun window-maximize ())
 "/session/{*active-session*}/window/maximize"
 :http-request-type post
 :http-body "{}");; required for some odd reason

;; 11.8.4 Minimize Window
(connect-to-endpoint
 (defun window-minimize ())
 "/session/{*active-session*}/window/minimize"
 :http-request-type post
 :http-body "{}");; again why?

;; 11.8.5 Fullscreen Window
(connect-to-endpoint
 (defun window-fullscreen ())
 "/session/{*active-session*}/window/fullscreen"
 :http-request-type post
 :http-body "{}")

;; 12.2.2 Find Element
(connect-to-endpoint
 (defun element-find (location-strategy selector)
   (declare (keyword location-strategy) (string selector)))
 "/session/{*active-session*}/element"
 :http-request-type post
 :http-body (jonathan.encode:to-json `(:obj (:|using| . ,location-strategy) (:|value| . ,selector))
                                     :from :jsown)
 :action (cadr body))

;; 12.2.3 Find Elements
(connect-to-endpoint
 (defun elements-find (location-strategy selector)
   (declare (keyword location-strategy) (string selector)))
 "/session/{*active-session*}/elements"
 :http-request-type post
 :http-body (jonathan.encode:to-json `(:obj (:|using| . ,location-strategy) (:|value| . ,selector))
                                     :from :jsown)
 :action (mapcar #'cadr body))

;; 12.2.4 Find Element From Element
(connect-to-endpoint
 (defun element-find-from-element (element-id location-strategy selector)
   (declare (keyword location-strategy) (string element-id selector)))
 "/session/{*active-session*}/element/{element-id}/element"
 :http-request-type post
 :http-body (jonathan.encode:to-json `(:obj (:|using| . ,location-strategy) (:|value| . ,selector))
                                     :from :jsown)
 :action (cadr body))

;; 12.2.5 Find Elements From Element
(connect-to-endpoint
 (defun elements-find-from-element (element-id location-strategy selector)
   (declare (keyword location-strategy) (string element-id selector)))
 "/session/{*active-session*}/element/{element-id}/elements"
 :http-request-type post
 :http-body (jonathan.encode:to-json `(:obj (:|using| . ,location-strategy) (:|value| . ,selector))
                                     :from :jsown)
 :action (mapcar #'cadr body))

;; 12.2.6 Get Active Element
(connect-to-endpoint
 (defun element-get-active ())
 "/session/{*active-session*}/element/active"
 :action (cadr body))

;; 12.3.1 Is Element Selected
;;todo test
(connect-to-endpoint
 (defun element-selected? (element-id)
   (declare (string element-id)))
 "/session/{*active-session*}/element/{element-id}/selected"
 :action
 body)

;; 12.3.2 Get Element Attribute
(connect-to-endpoint
 (defun element-attribute (element-id attr-name)
   (declare (string element-id attr-name)))
 "/session/{*active-session*}/element/{element-id}/attribute/{attr-name}")

;; 12.3.3 Get Element Property
(connect-to-endpoint
 (defun element-property (element-id prop-name)
   (declare (string element-id prop-name)))
 "/session/{*active-session*}/element/{element-id}/property/{prop-name}")

;; 12.3.4 Get Element CSS Value
(connect-to-endpoint
 (defun element-css (element-id prop-name)
   (declare (string element-id prop-name)))
 "/session/{*active-session*}/element/{element-id}/css/{prop-name}")

;; 12.3.5 Get Element Text
(connect-to-endpoint
 (defun element-text (element-id)
   (declare (string element-id)))
 "/session/{*active-session*}/element/{element-id}/text")

;; 12.3.6 Get Element Tag Name
(connect-to-endpoint
 (defun element-tag-name (element-id)
   (declare (string element-id)))
 "/session/{*active-session*}/element/{element-id}/name")

;; 12.3.7 Get Element Rect
(connect-to-endpoint
 (defun element-rect (element-id)
   (declare (string element-id)))
 "/session/{*active-session*}/element/{element-id}/rect")

;; 12.3.8 Is Element Enabled
(connect-to-endpoint
 (defun element-enabled? (element-id)
   (declare (string element-id)))
 "/session/{*active-session*}/element/{element-id}/enabled")

;; 12.3.9 Get Computed Role
;; 12.3.10 Get Computed Label
;; 12.4.1 Element Click
(connect-to-endpoint
 (defun element-click (element-id)
   (declare (string element-id)))
 "/session/{*active-session*}/element/{element-id}/click"
 :http-request-type post
 :http-body "{}"
 :action body)

;; 12.4.2 Element Clear
(connect-to-endpoint
 (defun element-clear (element-id)
   (declare (string element-id)))
 "/session/{*active-session*}/element/{element-id}/clear"
 :http-request-type post
 :http-body "{}") ;;again

;; 12.4.3 Element Send Keys
;;; U+E000 null for terminating a msg
;;; todo complete
(connect-to-endpoint
 (defun element-send-keys (element-id)
   (declare (string element-id)))
 "/session/{*active-session*}/element/{element-id}/value"
 :http-request-type post)

;; 13.1 Getting Page Source
(connect-to-endpoint
 (defun page-source ())
 "/session/{*active-session*}/source"
 :action body)

;; 13.2.1 Execute Script
(connect-to-endpoint
 (defun execute-script (args script))
 "/session/{*active-session*}/execute/sync"
 :http-request-type post
 :http-body (jonathan.encode:to-json `(:obj (:|script| . ,script) (:|args| . ,args))
                                     :from :jsown))

;; 13.2.2 Execute Async Script
;; 14.1 Get All Cookies
(connect-to-endpoint
 (defun cookies-get-all ())
 "/session/{*active-session*}/cookie")

;; 14.2 Get Named Cookie
(connect-to-endpoint
 (defun cookie-get (name)
   (declare (string name)))
 "/session/{*active-session*}/cookie/{name}")

;; 14.3 Add Cookie
;; 14.4 Delete Cookie
(connect-to-endpoint
 (defun cookie-delete (name)
   (declare (string name)))
 "/session/{*active-session*}/cookie/{name}"
 :http-request-type delete)

;; 14.5 Delete All Cookies
(connect-to-endpoint
 (defun cookie-delete-all ())
 "/session/{*active-session*}/cookie"
 :http-request-type delete)

;; 15.5 Perform Actions
;; 15.6 Release Actions
;; 16.1 Dismiss Alert
;; 16.2 Accept Alert
;; 16.3 Get Alert Text
;; 16.4 Send Alert Text
;; 17.1 Take Screenshot
(connect-to-endpoint
 (defun screenshot ())
 "/session/{*active-session*}/screenshot")

;; 17.2 Take Element Screenshot
(connect-to-endpoint
 (defun screenshot-element (element-id)
   (declare (string element-id)))
 "/session/{*active-session*}/element/{element-id}/screenshot")

;; 18. Print
(connect-to-endpoint
 (defun print-page ())
 "/session/{*active-session*}/print"
 :http-request-type post)
