(load "web-driver.lisp")

(quicklisp:quickload 'cl-arrows)
(use-package 'cl-arrows)

; Dont forget to start up a driver instance

(unless wd:*active-session*
  (wd:session-create))

(wd:navigate-to-url "https://en.wikipedia.org/wiki/Lisp_(programming_language)")

;; Grab the title of the wiki page.
(format t "~&Title: ~a" (wd:element-text (wd:element-find :|xpath| "//*[@id='firstHeading']")))

;; Count and show links on the page
(let ((elems (wd:elements-find :|xpath| "//a")))
  (format t "~&Number of links: ~a" (length elems))
  (format t "~&First 15 links: ~s" (mapcar #'wd:element-text (subseq elems 0 15)))
  (format t "~&Get Link of no.14 : ~a" (wd:element-attribute (nth 14 elems) "href")))


;; Wikipedia:Getting to Philosophy - Lazy Attempt
(let ((hp (LOOP
             :until (equalp "Philosophy" (wd:element-text (wd:element-find :|xpath| "//*[@id='firstHeading']")))
             :collect (wd:element-text (wd:element-find :|xpath| "//*[@id='firstHeading']"))
             :do (-> (wd:element-find :|xpath| "//*[@id='mw-content-text']//p/a[not(contains(@class, 'selflink') or contains(@href, 'Help:Pronunciation_respelling_key') or contains(@title, 'Latin'))]")
                     wd:element-click))))
  (format t "~&Hops to Philosophy: ~a ~s" (length hp) hp))


