# web-driver LISPifyed

A synchronous web scraping tool. Depends on quicklisp for package management. 
Old code recently resurrected from the dead for some simple web scraping. Currently in fluctuation and will be updated soon.

note: needs a web driver server running in the background to work.

## Example
```common_lisp
; Scrapping imaganary site
(load "web-driver.lisp")

(unless wd:*active-session*
    (wd:session-create))

(wd:navigate-to-url some-site)

(let ((products '()))
    (tagbody
     start
     (push
      (loop for product in (wd:elements-find :|xpath| "//*[@aria-label='Products In Category']/article")
            collect `((name . ,(wd:element-text (wd:element-find-from-element product :|xpath| ".//*[@itemprop='name']")))
                      (price . ,(-> (wd:element-find-from-element product :|xpath| ".//*[@itemprop='price']")
                                    wd:element-text
                                    (subseq 1)        ;remove dollar sign
                                    parse-number))
                             (brand . ,(intern (wd:element-attribute (wd:element-find-from-element product :|xpath| ".//meta[@itemprop='brand']")
                                                                     "content")))
                             (sku . ,(wd:element-attribute (wd:element-find-from-element product :|xpath| ".//meta[@itemprop='mpn']")
                                                           "content"))))
      filaments)
     (let ((btn-next (wd:element-find :|xpath| "//ul[@class='pagination']/li[last()]/a")))
       (when (not (equal (wd:element-attribute btn-next "aria-label")
                         "Current Page"))
         (wd:navigate-to-url (concatenate 'string some-url
                                          (wd:element-attribute btn-next "href")))
         (go start))))
    (mapcan #'identity products))

```
